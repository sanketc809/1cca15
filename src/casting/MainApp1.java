package casting;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner Sc1=new Scanner(System.in);
        System.out.println("ENTER QTY");
        int qty= Sc1.nextInt();
        System.out.println("ENTER PRICE");
        double price=Sc1.nextDouble();
        System.out.println("1:LAPTOP\n2:PROJECTOR");
        int choice=Sc1.nextInt();
        Machine m1=new Machine();
        if(choice==1){
            m1=new Laptop();
        } else if (choice==2) {
            m1 = new Projector();
        }
        m1.getType();
        m1.calculateBill(qty,price);


    }
}
