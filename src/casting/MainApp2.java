package casting;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Scanner Sc1=new Scanner(System.in);
        System.out.println("SELECTED SERVICE PROVIDER");
        System.out.println("1:AIRINDIA\n2:INDIGO");
        int choice=Sc1.nextInt();
        System.out.println("SELECT ROUTE");
        System.out.println("0:PUNE TO DELHI");
        System.out.println("1:MUMBAI TO CHENNAI");
        System.out.println("2:KOLKATA TO BANGALORE");
        int routeChoice=Sc1.nextInt();

        Goibibo g1=null;
        if (choice==1){
            g1=new AirIndia();
        } else if (choice==2) {
            g1=new Indigo();
        }
        assert g1 != null;
        g1.bookTicket(choice,routeChoice);
    }
}
