package casting;

public class Projector extends Machine {
    void getType() {
        System.out.println("Machine Type Is Projector");
    }

    void calculateBill(int qty, double price) {
        //10% gst
        double total = qty * price;
        double finalAmt = total + (total * 0.1);
        System.out.println("Final Amt :" + finalAmt);

    }
}
