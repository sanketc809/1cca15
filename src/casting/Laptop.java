package casting;

public class Laptop extends Machine {
    void getType(){
        System.out.println("Machine Type Is Laptop");
    }
    void calculateBill(int qty,double price){
        //15% gst
        double total=qty*price;
        double finalAmt=total+(total*0.15);
        System.out.println("Final Amt :"+finalAmt);

    }
}
