package Array;

public class ArrayDemo13 {
    public static void main(String[] args) {
        String[][] data = new String[3][2];
        data[0][0] = "Maharastra";
        data[0][1] = "Mumbai";
        data[1][0] = "Gujarat";
        data[1][1] = "Surat";
        data[2][0] = "Goa";
        data[2][1] = "Punji";

        for (int a = 0; a < data.length; a++) {
            for (int b = 0; b < data[a].length; b++) {
                System.out.print(data[a][b] + "\t");
            }
            System.out.println();
        }
    }
}
