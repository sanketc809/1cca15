package Array;

import java.util.Scanner;

public class ArrayDemo15 {
    public static void main(String[] args) {
        Scanner Sc1=new Scanner(System.in);
        System.out.println("ENTER TOTAL NO.OF FLOORS");
        int floors=Sc1.nextInt();
        System.out.println("ENTER TOTAL FLATS EACH FLOORS");
        int flats=Sc1.nextInt();

        int[][]data=new int[floors][flats];
        System.out.println("ENTER"+(floors*flats)+"FLATS NO.");

        for (int a=0;a<floors; a++){
            for (int b=0;b<flats;b++){
                data[a][b]=Sc1.nextInt();
            }
        }
        System.out.println("========================");
        for (int a=0;a<floors;a++){
            System.out.println("FLOORS NO."+(a+1));
            for (int b =0; b<flats; b++){
                System.out.print("FLATS NO."+data[a][b]+"\t");
            }
            System.out.println();
            System.out.println("=======================");
        }

    }
}
