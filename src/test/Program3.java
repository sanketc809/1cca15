package test;

import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("ENTER STRING");
        String S = sc.nextLine();
        int l = S.length();
        char first = S.charAt(0);
        char last = S.charAt(l - 1);
        String mid = S.substring(1, l - 1);
        String result = last + mid + first;
        System.out.println(result);

    }
}
