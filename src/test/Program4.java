package test;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Program4 {
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        System.out.println("ENTER NAME");
        String s=sc.nextLine();
        s=s.replace(" "," ");
        char arr[]=s.toCharArray();
        int count;
        Map<Character,Integer>map=new TreeMap<>();
        for (int i=0;i< arr.length;i++) {
            count = 0;
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    count++;
                }
            }
            map.put(arr[i], count);
        }

        System.out.println(map);
    }
}
