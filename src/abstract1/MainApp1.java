package abstract1;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Scanner Sc1=new Scanner(System.in);
        System.out.println("Select Type");
        System.out.println("1: Manager\n2: Watchman");
        int choice=Sc1.nextInt();
        Employee e=null;
        if (choice==1){
            e=new Manager();

        } else if (choice==2) {
            e=new Watchman();

        }
        e.getDesignation();
        e.getSalary();
    }
}
