package abstract1;

public abstract class Employee {
    abstract void getDesignation();
    abstract void getSalary();
}
