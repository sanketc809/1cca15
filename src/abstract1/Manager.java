package abstract1;

public class Manager extends Employee {

    @Override
    void getSalary() {
        System.out.println("Salary is 150000");
    }

    @Override
    void getDesignation() {
        System.out.println("Designation is Manager");
    }
}
