package Welcome;

import java.util.Scanner;

public class BankAccount {
    public static void main(String[] args) {
        Scanner Sc1 = new Scanner(System.in);
        System.out.println("Select Your Bank");
        System.out.println("1: BANK OF INDIA\t 2: HDFC\t 3:SBI");
        int choice = Sc1.nextInt();
        {
            if (choice == 1) {
                System.out.println("YOU SELECTED BANK OF INDIA");
            } else if (choice == 2) {
                System.out.println("YOU SELECTED HDFC");
            } else if (choice == 3) {
                System.out.println("YOU SELECTED SBI");
            } else {
                System.out.println("INVALID CHOICE ");
            }
            System.out.println("PLEASE FILL THE ACCOUNT REGISTER FORM");
            System.out.println("ENTER YOUR NAME");
            String name = Sc1.nextLine();
            String user = name.trim();

            if (!user.isEmpty()) {
                System.out.println("ENTER YOUR EMAIL");
                String email = Sc1.next();
                if (email.contains("@") && email.contains(".")) {
                    System.out.println("ENTER YOUR ADDRESS");
                    String add = Sc1.next();
                    if (!add.isEmpty()) {
                        System.out.println("ENTER YOUR PASSWORD");
                        String pass = Sc1.next();
                        if (pass.length() >= 8 && !(pass.contains(user)) && pass.contains("@")) {
                            System.out.println("ENTER CONFIRM PASSWORD");
                            String confirm = Sc1.next();
                            if (pass.equals(confirm)) {
                                System.out.println("WELCOME " + "\t" + user.toUpperCase());
                                System.out.println("YOUR ACCOUNT SUCCESSFULLY CREATED ");
                            } else {
                                System.out.println("INVALID CONFIRMS PASSWORD");
                            }
                        } else {
                            System.out.println("INVALID PASSWORD");
                        }
                    }
                }
            }
        }
    }
}

