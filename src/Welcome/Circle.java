package Welcome;

public class Circle {
  //  nonstatic double pi=3.14;
   static double pi=3.14;
    void area(double radius){
        double result=pi*radius*radius;
        System.out.println("Area of Circle: "+result);
    }
    void circumference(double radius){
        double result=2*pi*radius;
        System.out.println("Circumference of Circle :"+result);
    }
}
