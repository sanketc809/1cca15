package Welcome;

import java.util.Scanner;
public class ControlDemo {
    public static void main(String[] args) {
        Scanner Sc1 = new Scanner(System.in);
        System.out.println("Enter First Number");
        double num1 = Sc1.nextDouble();
        System.out.println("Enter Second Number");
        double num2 = Sc1.nextDouble();
        if (num1 > num2) {
            System.out.println("Number 1 is greater than 2");
        } else if (num2 > num1) {
            System.out.println("Number 2 is greater than 1");
        } else {
            System.out.println("Number 1 and Number 2 is Equal");
        }

    }
}
