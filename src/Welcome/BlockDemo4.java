package Welcome;

public class BlockDemo4 {
    static {
        System.out.println("static block");
    }

    public static void main(String[] args) {
        System.out.println("main method");
        BlockDemo4 d1=new BlockDemo4();
        BlockDemo4 d2=new BlockDemo4();
    }
    {

        System.out.println("non static block");
    }
}
