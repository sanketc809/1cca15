package Welcome;

public class MainApp1 {
    public static void main(String[] args) {
       // for static member access in different class.
        // class name as a ref. name
        System.out.println(Main.a);
        // for nonstatic member access in different class.
        // object should be created.
        Main m1=new Main();
        System.out.println(m1.b);
    }
}
