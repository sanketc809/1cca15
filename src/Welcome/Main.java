package Welcome;

public class Main {
    static int a = 30;
    double b = 40.25;
    static String c = "java";
//  To access static member in same class then not required any ref.
       // To access nonstatic member in same class then required to create object.
        public static void main(String[] args) {
            //nonstatic member in inside the method then no required to create object.
            int d=0;
            Main m = new Main();
            System.out.println(a);
            System.out.println(m.b);
            System.out.println(c);
            System.out.println(d);


        }
    }


