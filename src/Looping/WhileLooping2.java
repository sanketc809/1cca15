package Looping;
import java.util.Scanner;
public class WhileLooping2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        int no=0;
        int sum=0;
        while(sum<=50){
            System.out.println("ENTER NUMBER");
            no=sc1.nextInt();
            sum+=no;
        }
        System.out.println(sum);
    }
}
