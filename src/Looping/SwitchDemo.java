package Looping;
import javax.xml.bind.annotation.XmlType;
import java.util.Scanner;
public class SwitchDemo {
    public static void main(String[] args) {
        Scanner Sc1=new Scanner(System.in);
        System.out.println("SELECTED LANGUAGE");
        System.out.println("1:JAVA\n2:PYTHON\n3:PHP");
        int choice=Sc1.nextInt();
        switch (choice){
            case 1:
                System.out.println("SELECTED JAVA");
                break;
            case 2:
                System.out.println("SELECTED PYTHON");
                break;
            case 3:
                System.out.println("SELECTED PHP");
                break;
            default:
                System.out.println("INVALID CHOICE");

        }
    }
}
