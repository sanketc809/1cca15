package Looping;

public class Demo3 {
    public static void main(String[] args) {
       int lines=3;
       int star=1;
       for(int row=1;row<=lines;row++){
           for(int col=1;col<=star;col++){
               System.out.print("*"+"\t");
           }
           System.out.println();
           star++;
       }
    }
}
